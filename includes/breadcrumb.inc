<?php

/**
 * @file
 * Contains breadcrumb theme and related functions.
 */

use Drupal\Component\Utility\Html;

/**
 * Implements theme_breadrumb().
 *
 * Prints breadcrumbs as a list, with chevron divider.
 */
function semantic_ui_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $semantic_ui_breadcrumb = theme_get_setting('semantic_ui_breadcrumb');

  if ($semantic_ui_breadcrumb == 1 && !empty($breadcrumb)) {
    $breadcrumbs = '<h2 class="visually-hidden">' . t('You are here') . '</h2>';
    $breadcrumbs .= '<div class="ui list">';
    $breadcrumbs .= '<div class="ui breadcrumb top aligned">';
    //   echo "<pre>";
    //
    foreach ($breadcrumb as $value) {
      $breadcrumbs .= '<a href="' . $value['url'] . '"> ' . $value['text']->__toString() . '</a>' . '<i class="right chevron icon divider"></i>';
    }
    $request = \Drupal::request();
    $route_match = \Drupal::routeMatch();
    $page_title = \Drupal::service('title_resolver')
      ->getTitle($request, $route_match->getRouteObject());
    $title = Html::escape(strip_tags($page_title));
    $breadcrumbs .= '<div class="active section">' . $title . '</div>';
    $breadcrumbs .= '</div>';
    $breadcrumbs .= '</div>';
    return $breadcrumbs;
  }
}

/**
 * Implements hook_menu_breadcrumb_alter().
 */
function semantic_ui_menu_breadcrumb_alter(&$active_trail, $item) {
  foreach ($active_trail as $key => $item) {
    $active_trail[$key]['localized_options']['attributes']['class'][] = 'section';
  }
}
