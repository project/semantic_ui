/**
 * @file
 * Updates Drupal autocomplete to use Semantic UI styles.
 */

(function ($) {

  /**
   * Initializes Search selection on autocomplete fields.
   */
  Drupal.behaviors.autocompleteDropdown = {
    attach: function (context, settings) {
      var $dropdowns = $('.search.selection.dropdown');
      $dropdowns.each(function () {
        var $dropdown = $(this);
        $dropdown.dropdown({
          showOnFocus: false,
          selectOnKeydown: false,
          onChange: function (value, text, $choice) {
            $('input', $dropdown).trigger('autocompleteSelect', [value, text, $choice]);
          },
          apiSettings: {
            url: $dropdown.attr('data-autocomplete-path') + '/{query}',
            onResponse: function (drupalResponse) {
              if (!drupalResponse) {
                return;
              }

              var response = {
                results: []
              };
              for (var key in drupalResponse) {
                response.results.push({
                  'name': drupalResponse[key],
                  'value': key
                });
              }

              return response;
            }
          }
        });

        // Clear value if the field is empty and backspace or del key was hit.
        $dropdown.find('.search').on('keyup', function (event) {
          var keys = {
            backspace: 8,
            del: 46
          };
          if ($(this).val() == '' && (event.which == keys.backspace || event.which == keys.del)) {
            $dropdown.dropdown('clear');
          }
        });
      });
    }
  };

  Drupal.behaviors.autocompleteDisableSearch = {
    attach: function (context, settings) {
      var $autocompleteField = $('.field.disabled.autocomplete', context);
      if ($autocompleteField.length) {
        $('input.search', $autocompleteField).addClass('disabled').attr('disabled', 'disabled');
      }
    }
  };

})(jQuery);
