/**
 * @file
 * Updates onDrop method for block module to use Semantic UI behaviour.
 *
 * As default onDrop method just sets the value to the element we need to
 * notify Dropdown module from the Semantic UI framework that the element
 * was updated.
 */
(function ($) {
  Drupal.behaviors.semanticuiBlockDrag = {
    attach: function (context, settings) {
      var tableDrag = Drupal.tableDrag.blocks; // Get the blocks tableDrag object.

      // Add a handler so when a row is dropped, update fields dropped into new regions.
      tableDrag.onDrop = function () {
        dragObject = this;
        // Use "region-message" row instead of "region" row because
        // "region-{region_name}-message" is less prone to regexp match errors.
        var regionRow = $(dragObject.rowObject.element).prevAll('tr.region-message').get(0);
        var regionName = regionRow.className.replace(/([^ ]+[ ]+)*region-([^ ]+)-message([ ]+[^ ]+)*/, '$2');
        var regionField = $('select.block-region-select', dragObject.rowObject.element);
        // Check whether the newly picked region is available for this block.
        if ($('option[value=' + regionName + ']', regionField).length == 0) {
          // If not, alert the user and keep the block in its old region setting.
          alert(Drupal.t('The block cannot be placed in this region.'));
          // Simulate that there was a selected element change, so the row is put
          // back to from where the user tried to drag it.
          regionField.change();
        }
        else if ($(dragObject.rowObject.element).prev('tr').is('.region-message')) {
          var weightField = $('select.block-weight', dragObject.rowObject.element);
          var oldRegionName = weightField[0].className.replace(/([^ ]+[ ]+)*block-weight-([^ ]+)([ ]+[^ ]+)*/, '$2');

          if (!regionField.is('.block-region-' + regionName)) {
            regionField.removeClass('block-region-' + oldRegionName).addClass('block-region-' + regionName);
            weightField.removeClass('block-weight-' + oldRegionName).addClass('block-weight-' + regionName);
            regionField.val(regionName);
            regionField.change();
          }
          // Trigger behaviour only if region was changed as it triggers change
          // event and causes incorrent sort order.
          if (regionName !== oldRegionName) {
            $(regionField).parent('.ui.dropdown').dropdown('set selected');
          }
        }
      };
    }
  };
})(jQuery);
