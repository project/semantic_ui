/**
 * @file
   * Updates Comments body field to use popup for filter hint.
 */
(function ($) {
  Drupal.behaviors.semanticuiCommentsPopup = {
    attach: function (context, settings) {
      $('.comment-form .help.icon', context).popup({
        popup: $('.ui.popup.filter-wrapper'),
        delay: {
          hide: 200
        },
        hoverable: true
      });
    }
  };
})(jQuery);
