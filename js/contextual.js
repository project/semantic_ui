/**
 * @file
 * Updates default behaviors for the Contextual module.
 */

(function ($) {

  Drupal.contextualLinks = Drupal.contextualLinks || {};

  /**
   * Attaches outline behavior for regions associated with contextual links.
   */
  Drupal.behaviors.contextualLinks = {
    attach: function (context) {
      $('div.contextual-links-wrapper', context).once('contextual-links', function () {
        var $wrapper = $(this);
        var $region = $wrapper.closest('.contextual-links-region');
        var $links = $wrapper.find('div.contextual-links');
        var $trigger = $('<i class="large setting link icon contextual-links-trigger" />').hide();

        $region.hover(
          function() {
            $trigger.addClass('contextual-links-trigger-active').show();
          },
          function() {
            // Remove gear icon only in case if popup was not showed, otherwise
            // it will be hidden in onHidden callback.
            if ($trigger.popup('is hidden') === true) {
              $trigger.removeClass('contextual-links-trigger-active').hide();
            }
          }
          );
        // Prepend the trigger.
        $wrapper.prepend($trigger);
        $('<a href="#" />').addClass('visually-hidden').text(Drupal.t('Configure')).insertBefore($trigger);

        $trigger.popup({
          position: 'bottom left',
          hoverable: true,
          delay: {
            hide: 200
          }
        });

        // Hide gear icon after popup was hid, this behaviour prevents jumping
        // popup element when its parent is not visible.
        $trigger.popup('setting', 'onHidden', function() {
          $trigger.removeClass('contextual-links-trigger-active').hide();
        });
      });
    }
  };

})(jQuery);
