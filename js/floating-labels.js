/**
 * Updates field labels to use "floating label" behaviour.
 */
(function ($) {

  Drupal.semanticui = Drupal.semanticui || {};
  Drupal.semanticui.floatingLabels = Drupal.semanticui.floatingLabels || {
    duration: 700,
    delay: 100
  };

  /**
   * Attaches floating labels to text fields.
   */
  Drupal.behaviors.semanticuiTextFloatingLabels = {
    attach: function (context, settings) {
      // Initialise default label state.
      $('input[type=text], input[type=email], input[type=password]', context).each(function () {
        var $helperLabel = $(this).parent().find('.floating-label');
        if (!$helperLabel.length) {
          var $mainLabel = $(this).parent().find('label');
          $helperLabel = $mainLabel.clone();
          $helperLabel.addClass('floating-label').addClass('transition').addClass('hidden');
          $(this).parent().prepend($helperLabel);
          $mainLabel.addClass('main');
        }

        if ($(this).val() !== '' || $(this).is(':focus')) {
          $(this).parent().find('label.floating-label').removeClass('hidden');
          $(this).parent().find('label.main').addClass('transition').addClass('hidden');
        }
      });

      // Process focus and blur events.
      $('input[type=text], input[type=email], input[type=password]', context).each(function () {
        $(this).once('floating-label', function () {
          $(this).on('focus blur', function () {
            var $field = $(this).parent();
            if ($(this).val() !== '') {
              return;
            }

            $field.find('label.main').transition('fade');
            setTimeout(function () {
              $field.find('label.floating-label').transition({
                animation: 'fade',
                duration: Drupal.semanticui.floatingLabels.duration
              });
            }, Drupal.semanticui.floatingLabels.delay);
          });
        });
      });
    }
  };

  /**
   * Attaches floating labels to autocomplete dropdown fields.
   */
  Drupal.behaviors.semanticuiAutocompleteFloatingLabels = {
    attach: function (context, settings) {
      $('.autocomplete .dropdown > input[type=hidden]', context).each(function () {
        var $helperLabel = $(this).closest('.autocomplete.field').find('.floating-label');
        if (!$helperLabel.length) {
          var $mainLabel = $(this).closest('.autocomplete.field').find('label');
          $helperLabel = $mainLabel.clone();
          $helperLabel.addClass('floating-label').addClass('transition').addClass('hidden');
          $(this).closest('.autocomplete.field').prepend($helperLabel);
          $mainLabel.addClass('main');
        }

        if ($(this).val() !== '') {
          $(this).closest('.autocomplete.field').find('label.floating-label').removeClass('hidden');
          $(this).closest('.autocomplete.field').find('label.main').addClass('transition').addClass('hidden');
        }
      });

      // Catch "focusin" event as autocomplete dropdown may be initialized after
      // floating labels were attached. For example, when the field is loaded
      // with AJAX.
      var $autocompleteElements = $('.autocomplete > .dropdown');
      $autocompleteElements.each(function () {
        $(this).on('focusin', function () {
          var $searchElements = $(this).find('> input.search');
          $searchElements.each(function () {
            $(this).once('floating-label', function () {
              $(this).on('focus blur', function (event) {
                // Do not update label position if suggested value is being
                // selected from the dropdown menu.
                if (event.relatedTarget && $(event.relatedTarget).hasClass('menu')) {
                  return;
                }
                if ($(this).val() === '' && $(this).parent().find('input[type=hidden]').val() === '') {
                  var $field = $(this).closest('.autocomplete.field');
                  $field.find('label.main').transition('fade');
                  setTimeout(function () {
                    $field.find('label.floating-label').transition({
                      animation: 'fade',
                      duration: Drupal.semanticui.floatingLabels.duration
                    });
                  }, Drupal.semanticui.floatingLabels.delay);
                }
              });
            });
          });
        });
      });
    }
  };

  /**
   * Attaches floating labels to textarea fields.
   */
  Drupal.behaviors.semanticuiTextareaFloatingLabels = {
    attach: function (context, settings) {
      $('textarea', context).each(function () {
        var $helperLabel = $(this).closest('.form-type-textarea').find('.floating-label');
        if (!$helperLabel.length) {
          var $mainLabel = $(this).closest('.form-type-textarea').find('label');
          $helperLabel = $mainLabel.clone();
          $helperLabel.addClass('floating-label').addClass('transition').addClass('hidden');
          $(this).closest('.form-type-textarea').prepend($helperLabel);
          $mainLabel.addClass('main');
        }

        $helperLabel = $(this).closest('.webform-component-textarea').find('.floating-label');
        if (!$helperLabel.length) {
          $helperLabel = $(this).closest('.webform-component-textarea').find('label').clone();
          $helperLabel.addClass('floating-label').addClass('transition').addClass('hidden');
          $(this).closest('.webform-component-textarea').prepend($helperLabel);
        }

        if ($(this).val() !== '' || $(this).is(':focus')) {
          $(this).closest('.form-type-textarea').find('label.floating-label').removeClass('hidden');
          $(this).closest('.form-type-textarea').find('label.main').addClass('transition').addClass('hidden');

          $(this).closest('.webform-component-textarea').find('label.floating-label').removeClass('hidden');
          $(this).closest('.webform-component-textarea').find('label.main').addClass('transition').addClass('hidden');
        }
      });

      $('textarea', context).each(function () {
        $(this).once('floating-label', function () {
          $(this).on('focus blur', function () {
            if ($(this).val() !== '') {
              return;
            }

            var $regularField = $(this).closest('.form-type-textarea');
            $regularField.find('label.main').transition('fade');
            setTimeout(function () {
              $regularField.find('label.floating-label').transition({
                animation: 'fade',
                duration: Drupal.semanticui.floatingLabels.duration
              });
            }, Drupal.semanticui.floatingLabels.delay);

            var $webformField = $(this).closest('.webform-component-textarea');
            $webformField.find('label.main').transition('fade');
            setTimeout(function () {
              $webformField.find('label.floating-label').transition({
                animation: 'fade',
                duration: Drupal.semanticui.floatingLabels.duration
              });
            }, Drupal.semanticui.floatingLabels.delay);
          });
        });
      });
    }
  };

  /**
   * Attaches floating labels to fields with date.
   */
  Drupal.behaviors.semanticuiDatetimeFloatingLabels = {
    attach: function (context, settings) {
      var $helperLabel = $('.field-type-datetime .field > label.floating-label', context);
      if (!$helperLabel.length) {
        var $mainLabel = $('.field-type-datetime .field > label', context);
        $helperLabel = $mainLabel.clone();
        $helperLabel.addClass('floating-label').addClass('transition').addClass('hidden');
        $('.field-type-datetime .field', context).prepend($helperLabel);
        $mainLabel.addClass('main');
      }

      if ($('.field-type-datetime input', context).val() !== '' || $('.field-type-datetime input', context).is(':focus')) {
        $('.field-type-datetime .field > label.floating-label', context).removeClass('hidden');
        $('.field-type-datetime .field > label.main', context).addClass('transition').addClass('hidden');
      }


      $('.field-type-datetime input', context).each(function () {
        $(this).once('floating-label', function () {
          $('.field-type-datetime input', context).on('focus blur', function () {
            if ($(this).val() !== '') {
              return;
            }

            $('.field-type-datetime .field > label.main', context).transition('fade');
            setTimeout(function () {
              $('.field-type-datetime .field > label.floating-label', context).transition({
                animation: 'fade',
                duration: Drupal.semanticui.floatingLabels.duration
              });
            }, Drupal.semanticui.floatingLabels.delay);
          });
        });
      });
    }
  };

})(jQuery);
