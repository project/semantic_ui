/**
 * @file
 * Contains helper functions to work with Semantic UI theme.
 */

(function ($) {
  /**
   * Updates sticky header.
   */
  Drupal.behaviors.semanticuiSticky = {
    attach: function (context) {
      // As Drupal uses fixed position for the sticky header it takes maximum
      // width by default. When user scrolls page contents Drupal automatically
      // recalculates width, this behaviour has visible animation that is
      // useless. So to remove it we are precalculating widths for the sticky
      // header.
      var targetWidth = $('.sticky-header').next('table').css('width');

      // We are adding table classes here because sticky header is appending
      // with JavaScript (see file misc/tableheader.js).
      $('table.sticky-header', context).addClass('ui').addClass('sticky').addClass('table').css('width', targetWidth);
    }
  };

  Drupal.behaviors.semanticui = {
    attach: function (context) {
      // JavaScript for message.
      $('.message .close', context).on('click', function() {
        $(this).closest('.message').fadeOut();
      });

      // Check if a checkbox has proper wrapper.
      // There are cases when checkbox is rendered as simple HTML element
      // (e.g. at the Content page admin/content). But in the same time there
      // may be cases when checkbox is already properly rendered and there is
      // no need to add extra wrapper.
      if (!$('.form-checkbox', '.ui.table th').parent('.ui.checkbox').length) {
        // Append wrapper and label elements to make Semantic UI
        // checkboxes working.
        $('.form-checkbox', '.ui.table th').wrap('<div class="ui checkbox"></div>');
        $('.ui.checkbox', '.ui.table th').append('<label></label>');
      }

      // Append label element to the checkbox input as Semantic UI uses it to
      // display the checkbox.
      // Semantic UI framework will add label only if checkbox does not have
      // one, so we are filtering invisible elements and appending the label.
      $('.ui.checkbox.tab-label', context).once('field-label', function() {
        if ($('label.visually-hidden', this).length) {
          $(this).append('<label>');
        }
      });

      // Initialize checkboxes and set onChange callback. It is required because
      // Drupal uses exactly "click" event (see file misc/tableselect.js) when
      // Semantic UI checkbox module triggers only "change" event.
      $('.ui.checkbox', context).checkbox('setting', 'onChange', function() {
        $(this).closest('tr').toggleClass('active', $(this).get(0).checked);
        $(this).triggerHandler('click');
      });

      $('.ui.checkbox', '.ui.table th').checkbox('setting', 'onChange', function() {
        var e = jQuery.Event("click");
        e.target = this;
        $(this).triggerHandler(e);
        // Execute "click" handlers, attached to all parents elements.
        $(this).parentsUntil('.ui.table tr').each(function () {
          $(this).triggerHandler(e);
        });
      });

      // The main logic for handling "selected" class is in the file
      // tableselect.js and we just add Semantic UI "active" class depending
      // from "select" one.
      $('.ui.checkbox', '.ui.table th.select-all').checkbox('setting', 'onChecked', function() {
        $('tbody > tr', $(this).closest('table')).addClass('active');
        $('tbody > tr', $(this).closest('table').next('table')).addClass('active');
      });
      $('.ui.checkbox', '.ui.table th.select-all').checkbox('setting', 'onUnchecked', function() {
        $('tbody > tr', $(this).closest('table')).removeClass('active');
        $('tbody > tr', $(this).closest('table').next('table')).removeClass('active');
      });

      var isPermissionsPage = $('table#permissions', context).length;
      // Wrap checkbox element into the div wrapper so by default it will
      // be displayed as block element. Skip page with permissions as it uses
      // dummy checkboxes for roles other than "Anonymous user" and
      // "Authenticated user". Checkboxes for these roles do not use bottom
      // margin as they are "last" elements while checkboxes for other roles use
      // margin because of dummy checkbox.
      if (!isPermissionsPage) {
        $('.ui.checkbox.form-item', context).once('field-wrapper', function() {
          $(this).wrap('<div class="field"></div>');
        });
      }
    }
  };

  /**
   * Applies Dropdown module for each select element.
   *
   * Here is additional check for AJAX presence, it is used to properly
   * handle Drupal ajax classes with Semantic UI framework.
   *
   * Semantic UI creates div wrapper for select element, then applies
   * select's classes to this wrapper and finally removes all classes
   * from the select element. Such behaviour leads to situation when AJAX
   * callback will be called twice. It happens because Drupal
   * behaviours may be called multiple times and during first call in
   * $(element).dropdown() Semantic UI will remove element's classes.
   * After that Drupal will see (in the file misc/ajax.js in
   * Drupal.behaviors.AJAX) that there is an element with id that uses AJAX
   * but does not have AJAX class and will bind callback function to the
   * event that was specified in the FAPI #ajax element configuration
   * (e.g. 'change'). This will be second binding and callback will be
   * called twice that may break UI. See how Drupal binds the ajaxSubmit
   * function to the element event in the file misc/ajax.js in Drupal.ajax.
   */
  Drupal.behaviors.semanticuiDropdown = {
    attach: function(context) {
      $('select.ui.dropdown', context).each(function() {
        // Create empty label if it is absent as Semantic UI assumes that
        // dropdown is initialized if there is no previous element. In some
        // cases related label may be directly unset which causes the element to
        // be hidden and uninitialized.
        if (!$(this).parent().find('label').length) {
          $(this).parent().prepend($('<label>').addClass('visually-hidden'));
        }

        var hasAjax = $(this).hasClass('ajax-processed');
        // Region and weight classes are used at least on the Blocks page and
        // are required by blockDrag behavior.
        var hasBlockRegion = $(this).hasClass('block-region-select');
        var regionClasses = null;
        var classesToRemove = ['ui', 'dropdown'];
        if (hasBlockRegion) {
          regionClasses = $(this).attr('class').split(/\s+/);
          regionClasses = $.grep(regionClasses, function(regionClass) {
            return $.inArray(regionClass, classesToRemove) === -1;
          }, false);
        }

        var hasBlockWeight = $(this).hasClass('block-weight');
        var weightClasses = null;
        if (hasBlockWeight) {
          weightClasses = $(this).attr('class').split(/\s+/);
          weightClasses = $.grep(weightClasses, function(weightClass) {
            return $.inArray(weightClass, classesToRemove) === -1;
          }, false);
        }

        // Semantic UI triggers .change event during the initialization phase.
        // With this behaviour AJAX select element will generate infinite
        // requests to the backend. Here is a short algorithm:
        // 1. user selects some value
        // 2. AJAX generates request to the backend
        // 3. backend responds with updated form elements
        // 4. Drupal calls registered behaviours
        // 5. semanticuiDropdown behaviour updates select element to use
        //    Semantic UI Dropdown module
        // 6. event .change is called during initialization the module
        // 7. things are being repeated from step 2
        // To remove infinite requests it would be good to unbind .change
        // events, then initialize Dropdown module and then assign
        // back .change events.
        if (hasAjax) {
          var selectEvents = $._data(this, 'events');
          var selectEventsCopy = $.extend(true, {}, selectEvents);
          $(this).unbind('change');
        }

        // Convert HTML select element to Semantic UI Dropdown.
        $(this).dropdown({
          selectOnKeydown: false
        });

        // Assign back change events.
        if (hasAjax) {
          var selectElement = this;
          $.each(selectEventsCopy, function() {
            $.each(this, function() {
              if (this.type != 'change') {
                return;
              }
              $(selectElement).bind(this.type, this.handler);
            });
          });

          // Apply required classes to select element.
          $(this).addClass('ajax-processed');
        }

        // Assign required classes to the select element and process its
        // wrapper.
        // At the Blocks page Drupal uses classes from the select element, so
        // it is required to remove them from the wrapper div that is added by
        // Semantic UI framework.
        if (hasBlockRegion) {
          $(this).addClass(regionClasses.join(' '));
          $(this).parent().removeClass(regionClasses.join(' '));
        }
        if (hasBlockWeight) {
          $(this).addClass(weightClasses.join(' '));
          $(this).parent().removeClass(weightClasses.join(' '));
        }
      });
    }
  };

  /**
   * Initializes dropdown buttons.
   */
  Drupal.behaviors.semanticuiDropdownButton = {
    attach: function(context) {
      $('.ui.dropdown.button', context).dropdown();
    }
  };

  /**
   * Changes caret icon for fieldset when it is being collapsed or expanded.
   */
  Drupal.behaviors.semanticuiFieldset = {
    attach: function(context) {
      $('fieldset', context).on('collapsed', function(data) {
        $caretIcon = $('.caret.icon', this);
        if (data.value === true) {
          $caretIcon.removeClass('down').addClass('right');
        }
        else {
          $caretIcon.removeClass('right').addClass('down');
        }
      });
    }
  };

  /**
   * Adds Semantic UI Popup Module to fields descriptions.
   */
  Drupal.behaviors.semanticuiPopup = {
    attach: function(context) {
      $('.ui.form [data-html]', context).popup({
        hoverable: true,
        delay: {
          show: 300,
          hide: 800
        }
      });
    }
  };

  /**
   * Submit search form when user clicks on search icon.
   */
  Drupal.behaviors.semanticuiSearch = {
    attach: function(context) {
      $('.ui.search > .icon', context).click(function() {
        $(this).closest('form').submit();
      });
    }
  };

})(jQuery);
