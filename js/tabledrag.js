/**
 * @file
 * Take an item and add event handlers to make it become draggable.
 *
 * Overrides default makeDraggable method with new tabledrag handle markup.
 */
(function ($) {
  Drupal.tableDrag.prototype.makeDraggable = function (item) {
    var self = this;

    // Create the handle.
    var handle = $('<i class="tabledrag-handle move link icon"></i>').attr('title', Drupal.t('Drag to re-order'));
    // Insert the handle after indentations (if any).
    if ($('td:first .indentation:last', item).length) {
      $('td:first .indentation:last', item).after(handle);
      // Update the total width of indentation in this entire table.
      self.indentCount = Math.max($('.indentation', item).length, self.indentCount);
    }
    else {
      $('td:first', item).prepend(handle);
    }

    // Add hover action for the handle.
    handle.hover(function () {
      self.dragObject == null ? $(this).addClass('tabledrag-handle-hover') : null;
    }, function () {
      self.dragObject == null ? $(this).removeClass('tabledrag-handle-hover') : null;
    });

    // Add the mousedown action for the handle.
    handle.mousedown(function (event) {
      // Create a new dragObject recording the event information.
      self.dragObject = {};
      self.dragObject.initMouseOffset = self.getMouseOffset(item, event);
      self.dragObject.initMouseCoords = self.mouseCoords(event);
      if (self.indentEnabled) {
        self.dragObject.indentMousePos = self.dragObject.initMouseCoords;
      }

      // If there's a lingering row object from the keyboard, remove its focus.
      if (self.rowObject) {
        $('a.tabledrag-handle', self.rowObject.element).blur();
      }

      // Create a new rowObject for manipulation of this row.
      self.rowObject = new self.row(item, 'mouse', self.indentEnabled, self.maxDepth, true);

      // Save the position of the table.
      self.table.topY = $(self.table).offset().top;
      self.table.bottomY = self.table.topY + self.table.offsetHeight;

      // Add classes to the handle and row.
      $(this).addClass('tabledrag-handle-hover');
      $(item).addClass('drag');

      // Set the document to use the move cursor during drag.
      $('body').addClass('drag');
      if (self.oldRowElement) {
        $(self.oldRowElement).removeClass('drag-previous');
      }

      // Hack for IE6 that flickers uncontrollably if select lists are moved.
      if (navigator.userAgent.indexOf('MSIE 6.') != -1) {
        $('select', this.table).css('display', 'none');
      }

      // Hack for Konqueror, prevent the blur handler from firing.
      // Konqueror always gives links focus, even after returning false on mousedown.
      self.safeBlur = false;

      // Call optional placeholder function.
      self.onDrag();
      return false;
    });

    // Prevent the anchor tag from jumping us to the top of the page.
    handle.click(function () {
      return false;
    });

    // Similar to the hover event, add a class when the handle is focused.
    handle.focus(function () {
      $(this).addClass('tabledrag-handle-hover');
      self.safeBlur = true;
    });

    // Remove the handle class on blur and fire the same function as a mouseup.
    handle.blur(function (event) {
      $(this).removeClass('tabledrag-handle-hover');
      if (self.rowObject && self.safeBlur) {
        self.dropRow(event, self);
      }
    });

    // Add arrow-key support to the handle.
    handle.keydown(function (event) {
      // If a rowObject doesn't yet exist and this isn't the tab key.
      if (event.keyCode != 9 && !self.rowObject) {
        self.rowObject = new self.row(item, 'keyboard', self.indentEnabled, self.maxDepth, true);
      }

      var keyChange = false;
      switch (event.keyCode) {
        case 37: // Left arrow.
        case 63234: // Safari left arrow.
          keyChange = true;
          self.rowObject.indent(-1 * self.rtl);
          break;
        case 38: // Up arrow.
        case 63232: // Safari up arrow.
          var previousRow = $(self.rowObject.element).prev('tr').get(0);
          while (previousRow && $(previousRow).is(':hidden')) {
            previousRow = $(previousRow).prev('tr').get(0);
          }
          if (previousRow) {
            self.safeBlur = false; // Do not allow the onBlur cleanup.
            self.rowObject.direction = 'up';
            keyChange = true;

            if ($(item).is('.tabledrag-root')) {
              // Swap with the previous top-level row.
              var groupHeight = 0;
              while (previousRow && $('.indentation', previousRow).length) {
                previousRow = $(previousRow).prev('tr').get(0);
                groupHeight += $(previousRow).is(':hidden') ? 0 : previousRow.offsetHeight;
              }
              if (previousRow) {
                self.rowObject.swap('before', previousRow);
                // No need to check for indentation, 0 is the only valid one.
                window.scrollBy(0, -groupHeight);
              }
            }
            else if (self.table.tBodies[0].rows[0] != previousRow || $(previousRow).is('.draggable')) {
              // Swap with the previous row (unless previous row is the first one
              // and undraggable).
              self.rowObject.swap('before', previousRow);
              self.rowObject.interval = null;
              self.rowObject.indent(0);
              window.scrollBy(0, -parseInt(item.offsetHeight, 10));
            }
            handle.get(0).focus(); // Regain focus after the DOM manipulation.
          }
          break;
        case 39: // Right arrow.
        case 63235: // Safari right arrow.
          keyChange = true;
          self.rowObject.indent(1 * self.rtl);
          break;
        case 40: // Down arrow.
        case 63233: // Safari down arrow.
          var nextRow = $(self.rowObject.group).filter(':last').next('tr').get(0);
          while (nextRow && $(nextRow).is(':hidden')) {
            nextRow = $(nextRow).next('tr').get(0);
          }
          if (nextRow) {
            self.safeBlur = false; // Do not allow the onBlur cleanup.
            self.rowObject.direction = 'down';
            keyChange = true;

            if ($(item).is('.tabledrag-root')) {
              // Swap with the next group (necessarily a top-level one).
              var groupHeight = 0;
              var nextGroup = new self.row(nextRow, 'keyboard', self.indentEnabled, self.maxDepth, false);
              if (nextGroup) {
                $(nextGroup.group).each(function () {
                  groupHeight += $(this).is(':hidden') ? 0 : this.offsetHeight;
                });
                var nextGroupRow = $(nextGroup.group).filter(':last').get(0);
                self.rowObject.swap('after', nextGroupRow);
                // No need to check for indentation, 0 is the only valid one.
                window.scrollBy(0, parseInt(groupHeight, 10));
              }
            }
            else {
              // Swap with the next row.
              self.rowObject.swap('after', nextRow);
              self.rowObject.interval = null;
              self.rowObject.indent(0);
              window.scrollBy(0, parseInt(item.offsetHeight, 10));
            }
            handle.get(0).focus(); // Regain focus after the DOM manipulation.
          }
          break;
      }

      if (self.rowObject && self.rowObject.changed == true) {
        $(item).addClass('drag');
        if (self.oldRowElement) {
          $(self.oldRowElement).removeClass('drag-previous');
        }
        self.oldRowElement = item;
        self.restripeTable();
        self.onDrag();
      }

      // Returning false if we have an arrow key to prevent scrolling.
      if (keyChange) {
        return false;
      }
    });

    // Compatibility addition, return false on keypress to prevent unwanted scrolling.
    // IE and Safari will suppress scrolling on keydown, but all other browsers
    // need to return false on keypress. http://www.quirksmode.org/js/keys.html
    handle.keypress(function (event) {
      switch (event.keyCode) {
        case 37: // Left arrow.
        case 38: // Up arrow.
        case 39: // Right arrow.
        case 40: // Down arrow.
          return false;
      }
    });
  };

})(jQuery);
