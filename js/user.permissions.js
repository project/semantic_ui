(function ($) {

  /**
   * Overrides default behaviour and adds support of Semantic UI markup.
   */
  Drupal.behaviors.permissions = {
    attach: function (context) {
      var self = this;
      $('table#permissions').once('permissions', function () {
        // On a site with many roles and permissions, this behavior initially has
        // to perform thousands of DOM manipulations to inject checkboxes and hide
        // them. By detaching the table from the DOM, all operations can be
        // performed without triggering internal layout and re-rendering processes
        // in the browser.
        var $table = $(this);
        if ($table.prev().length) {
          var $ancestor = $table.prev(), method = 'after';
        }
        else {
          var $ancestor = $table.parent(), method = 'append';
        }
        $table.detach();

        // Create dummy checkboxes. We use dummy checkboxes instead of reusing
        // the existing checkboxes here because new checkboxes don't alter the
        // submitted form. If we'd automatically check existing checkboxes, the
        // permission table would be polluted with redundant entries. This
        // is deliberate, but desirable when we automatically check them.
        var $dummy = $('<div class="ui checkbox"><input type="checkbox" class="dummy-checkbox" disabled="disabled" checked="checked" /></div>')
        .append('<label></label>')
        .attr('title', Drupal.t("This permission is inherited from the authenticated user role."))
        .hide();

        $('input[type=checkbox]', this).not('.rid-2, .rid-1').addClass('real-checkbox').each(function () {
          $dummy.clone().insertAfter($(this).parent());
        });

        // Initialize the authenticated user checkbox.
        $('input[type=checkbox].rid-2', this)
        .bind('click.permissions', self.toggle)
        // .triggerHandler() cannot be used here, as it only affects the first
        // element.
        .each(self.toggle);

        // Re-insert the table into the DOM.
        $ancestor[method]($table);
      });
    },

    /**
     * Toggles all dummy checkboxes based on the checkboxes' state.
     *
     * If the "authenticated user" checkbox is checked, the checked and disabled
     * checkboxes are shown, the real checkboxes otherwise.
     */
    toggle: function () {
      var authCheckbox = this, $row = $(this).closest('tr');
      $row.find('.real-checkbox').each(function () {
        $(this).parent('.ui.checkbox').get(0).style.display = (authCheckbox.checked ? 'none' : '');
      });
      $row.find('.dummy-checkbox').each(function () {
        $(this).parent('.ui.checkbox').get(0).style.display = (authCheckbox.checked ? '' : 'none');
      });
    }
  };

})(jQuery);
