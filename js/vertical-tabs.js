/**
 * @file
 * Contains helper functionality for updating default Drupal vertical tabs to
 * Semantic UI vertical menu.
 */

(function ($) {
  /**
   * Overwrites default Drupal verticalTabs behaviour.
   *
   * Integrates Semantic UI vertical menu instead of Drupal vertical tabs.
   */
  Drupal.behaviors.verticalTabs = {
    attach: function (context) {
      $('.vertical-tabs-panes', context).once('vertical-tabs', function () {
        var focusID = $(':hidden.vertical-tabs-active-tab', this).val();
        var tab_focus;

        // Check if there are some fieldsets that can be converted to vertical-tabs
        var $fieldsets = $('> fieldset', this);
        if ($fieldsets.length == 0) {
          return;
        }

        // Create the tab column.
        var tab_list = $('<div class="ui attached vertical tabular menu"></div>');
        var tab_list_wrapper = $('<div class="four wide column"></div>').append(tab_list);

        $(this).wrap('<div class="ui basic vertical segment vertical-menu"></div>').wrap('<div class="ui grid"></div>').before(tab_list_wrapper);
        $(this).addClass('twelve').addClass('wide').addClass('column');

        // Transform each fieldset into a tab.
        $fieldsets.each(function () {
          var vertical_tab = new Drupal.verticalTab({
            title: $('> legend', this).text(),
            fieldset: $(this)
          });
          tab_list.append(vertical_tab.item);
          $(this)
            .removeClass('collapsible collapsed')
            .addClass('vertical-tabs-pane')
            .data('verticalTab', vertical_tab);
          if (this.id == focusID) {
            tab_focus = $(this);
          }
        });

        $('> li:first', tab_list).addClass('first');
        $('> li:last', tab_list).addClass('last');

        if (!tab_focus) {
          // If the current URL has a fragment and one of the tabs contains an
          // element that matches the URL fragment, activate that tab.
          if (window.location.hash && $(this).find(window.location.hash).length) {
            tab_focus = $(this).find(window.location.hash).closest('.vertical-tabs-pane');
          }
          else {
            tab_focus = $('> .vertical-tabs-pane:first', this);
          }
        }
        if (tab_focus.length) {
          tab_focus.data('verticalTab').focus();
        }
      });
    }
  };

  /**
   * Theme function for a vertical tab.
   *
   * Uses Semantic UI markup to draw vertical menu.
   */
  Drupal.theme.verticalTab = function (settings) {
    var tab = {};
    tab.link = $('<a class="item" href="#"></a>')
      .append(tab.title = $('<strong></strong>').text(settings.title))
      .append(tab.summary = $('<div class="summary"></div>')
      );
    tab.item = tab.link;
    return tab;
  };

  Drupal.verticalTab.prototype.focus = function () {
    this.fieldset
      .siblings('fieldset.vertical-tabs-pane')
        .each(function () {
          var tab = $(this).data('verticalTab');
          tab.fieldset.hide();
          tab.item.removeClass('active');
        })
        .end()
      .show()
      .siblings(':hidden.vertical-tabs-active-tab')
        .val(this.fieldset.attr('id'));
    this.item.addClass('active');
    // Mark the active tab for screen readers.
    $('#active-vertical-tab').remove();
    this.link.append('<span id="active-vertical-tab" class="visually-hidden">' + Drupal.t('(active tab)') + '</span>');
  }
})(jQuery);
