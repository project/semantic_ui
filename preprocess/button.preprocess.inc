<?php

/**
 * @file
 * Implements preprocess functions for Semantic UI button styles.
 */

use Drupal\Component\Utility\Unicode;

/**
 * Implements hook_preprocess_button().
 */
function semantic_ui_preprocess_button(&$vars) {
  $vars['element']['#attributes']['class'][] = 'button';
  $vars['element']['#attributes']['class'][] = 'ui';
  if (isset($vars['element']['#value'])) {
    if ($class = semantic_ui_colorize_button($vars['element']['#value'])) {
      $vars['element']['#attributes']['class'][] = $class;
    }
  }
}

/**
 * Provides button class depending from its value.
 */
function semantic_ui_colorize_button($text) {
  $generic_strings = [
    'button'   => [
      t('Save'),
      t('Confirm'),
      t('Submit'),
      t('Search'),
    ],
    'positive' => [
      t('Add'),
      t('Create'),
      t('Write'),
    ],
    'orange'   => [
      t('Export'),
      t('Import'),
      t('Restore'),
      t('Rebuild'),
    ],
    'button'   => [
      t('Apply'),
      t('Update'),
    ],
    'negative' => [
      t('Delete'),
      t('Remove'),
    ],
  ];

  // Generic matching last.
  foreach ($generic_strings as $class => $strings) {
    foreach ($strings as $string) {
      if (mb_strtolower($text) == mb_strtolower($string)) {
        return $class;
      }
    }
  }
  return FALSE;
}
