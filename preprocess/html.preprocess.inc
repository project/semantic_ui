<?php

/**
 * @file
 * Override or insert variables into the HTML templates.
 *
 * To add dynamically your own classes use
 * $vars['classes_array'][] = 'my_class';
 */

use Drupal\Core\Render\Markup;

/**
 * Implements hook_preprocess_html().
 */
function semantic_ui_preprocess_html(&$vars) {
  if (theme_get_setting('semantic_ui_floating_labels')) {
    // Add floating-labels JS/CSS.
    $vars['#attached']['library'][] = 'semantic_ui/floating-labels';
  }
  if (theme_get_setting('semantic_ui_enlarge_touchable_font')) {
    $vars['#attached']['library'][] = 'semantic_ui/touchable-css';
    // Add detect touchable js to header.
    $detect_touchable_js = '
    (function () {
     var isTouchable = ("ontouchstart" in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);
     if (isTouchable) {
      document.getElementsByTagName("html")[0].classList.add("touchable");
      }
      })();';
    $vars['#attached']['html_head'][] = [
      // The data.
      [
        // Add a <script> tag.
        '#tag'    => 'script',
        // Add JavaScript to the <script> tag.
        '#value'  => Markup::create($detect_touchable_js),
        // Give weight so it appears after meta tags, etc.
        '#weight' => -1,
      ],
      // A key, to make it possible to recognize this HTML <HEAD> element when altering.
      'semantic_uiDetectTouchable',
    ];
  }
}
