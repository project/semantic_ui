<?php

/**
 * @file
 * Contains preprocess functions for image.
 */

use Drupal\Component\Utility\Html;

/**
 * Implements hook_preprocess_image().
 *
 * Removes height and width inline styles from images to make them responsive.
 */
function semantic_ui_preprocess_image(&$variables) {
  foreach (['width', 'height'] as $key) {
    unset($variables[$key]);
  }

  if (empty($variables['attributes']['class'])) {
    $variables['attributes']['class'] = [];
  }
  elseif (!is_array($variables['attributes']['class'])) {
    $variables['attributes']['class'] = explode(' ', $variables['attributes']['class']);
  }

  if (isset($variables['style_name'])) {
    $variables['attributes']['class'][] = 'ui';
    $variables['attributes']['class'][] = 'image';
    $variables['attributes']['class'][] = Html::cleanCssIdentifier($variables['style_name']);
  }
}
