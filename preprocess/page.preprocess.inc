<?php

/**
 * @file
 * Preprocess functions for page.
 */

use Drupal\Core\Render\Element;

/**
 * Implements hook_preprocess_page().
 */
function semantic_ui_preprocess_page(&$variables) {
  $site_config = \Drupal::config('system.site');
  $variables['site_name'] = $site_config->get('name');
  $variables['site_slogan'] = $site_config->get('slogan');
  // logo needs to be placed within specified folder
  $variables['logo'] = file_url_transform_relative(file_create_url(theme_get_setting('logo.url')));

  if (!empty($variables['page']['sidebar_first'])) {
    $left = $variables['page']['sidebar_first'];
  }

  if (!empty($variables['page']['sidebar_second'])) {
    $right = $variables['page']['sidebar_second'];
  }
  // Dynamic sidebars.
  if (!empty($left) && !empty($right)) {
    $variables['main_grid'] = 'eight wide column';
    $variables['sidebar_left'] = 'four wide column';
    $variables['sidebar_right'] = 'four wide column';
  }
  elseif (empty($left) && !empty($right)) {
    $variables['main_grid'] = 'twelve wide column';
    $variables['sidebar_left'] = '';
    $variables['sidebar_right'] = 'four wide column';
  }
  elseif (!empty($left) && empty($right)) {
    $variables['main_grid'] = 'twelve wide column';
    $variables['sidebar_left'] = 'four wide column';
    $variables['sidebar_right'] = '';
  }
  else {
    $variables['main_grid'] = 'sixteen wide column';
    $variables['sidebar_left'] = '';
    $variables['sidebar_right'] = '';
  }

  $variables['primary_navigation'] = NULL;
  if (!empty($variables['main_menu']) && \Drupal::state()->get('menu_main_links_source', FALSE)) {
    $variables['primary_navigation'] = menu_tree(\Drupal::state()->get('menu_main_links_source'));
    // Unset menu wrapper as it is rendered as right part in exising
    // menu (see template page.tpl.php).
    $variables['primary_navigation']['#theme_wrappers'] = [];
  }

  $variables['secondary_navigation'] = NULL;
  if (!empty($variables['secondary_menu']) && \Drupal::state()->get('menu_secondary_links_source', FALSE)) {
    $variables['secondary_navigation'] = menu_tree(\Drupal::state()->get('menu_secondary_links_source'));
    $variables['secondary_navigation']['#theme_wrappers'] = ['menu_tree__secondary'];
  }

  // Update menus in sidebars to use separate wrapper.
  iscale_page_update_sidebar_menus($variables);

  // As we are placing secondary menu inside the wrapper we need to split menus
  // into two variables.
  $variables['tabs_primary'] = $variables['tabs'];
  unset($variables['tabs_primary']['#secondary']);

  $variables['tabs_secondary'] = $variables['tabs'];
  unset($variables['tabs_secondary']['#primary']);

  // Making fixed navigation bar if this option is enabled.
  $semantic_ui_fixed_navbar = theme_get_setting('semantic_ui_fixed_navbar');
  if ($semantic_ui_fixed_navbar == 1) {
    $variables['fixed_navbar'] = 'fixed overlay-displace-top';
    drupal_add_js(drupal_get_path('theme', 'semantic_ui') . '/js/displace.js', ['group' => JS_THEME]);

    $access_administration_menu = module_exists('admin_menu') && \Drupal::currentUser()->hasPermission('access administration menu') && !admin_menu_suppress(FALSE);
    if ($access_administration_menu && \Drupal::state()->get('admin_menu_position_fixed', 0)) {

      $variables['fixed_navbar'] .= ' admin-menu';
    }
  }
  else {
    $variables['fixed_navbar'] = 'attached';
  }
}

/**
 * Update menu if it is placed in a sidebar.
 *
 * By default Drupal renders block title above the block. It would be good to
 * render it as a menu title inside menu structure (with classes "header item").
 *
 * @param array $variables
 *   An array of variables passed from hook_preprocess_page().
 */
function iscale_page_update_sidebar_menus(&$variables) {
  $menus = menu_ui_get_menus(TRUE);
  $sidebars = ['sidebar_first', 'sidebar_second'];

  // Walk through both sidebars.
  foreach ($sidebars as $sidebar) {
    $sidebar_elements = isset($variables['page'][$sidebar]) ? Element::children($variables['page'][$sidebar]) : [];
    foreach ($sidebar_elements as $element_name) {
      $element = $variables['page'][$sidebar][$element_name];

      // Check if sidebar element is a menu.
      if (isset($element['#block']) &&
        in_array($element['#block']->module, ['system', 'menu']) &&
        array_key_exists($element['#block']->delta, $menus)) {

        // Do not add title item if it is empty.
        if (!empty($variables['page'][$sidebar][$element_name]['#block']->subject)) {
          $menu_header['menu_header'] = [
            '#type'       => 'container',
            '#attributes' => [
              'class' => [
                'header',
                'item',
              ],
            ],
          ];
          $menu_header['menu_header']['header'] = [
            '#markup' => $variables['page'][$sidebar][$element_name]['#block']->subject,
          ];

          $variables['page'][$sidebar][$element_name] = $menu_header + $variables['page'][$sidebar][$element_name];
        }

        $variables['page'][$sidebar][$element_name]['#is_menu'] = TRUE;
      }
    }
  }
}
