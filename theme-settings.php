<?php

/**
 * @file
 * Provides theme settings for Semantic based themes.
 */

use Drupal\Core\Form\FormState;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function semantic_ui_form_system_theme_settings_alter(&$form, FormState &$form_state) {
  $form['semantic_ui'] = [
    '#prefix' => '<h2 class="ui dividing header">' . t('Semantic UI Settings') . '</h2>',
  ];

  $form['semantic_ui']['semantic_ui_breadcrumb'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Enable breadcrumbs'),
    '#default_value' => theme_get_setting('semantic_ui_breadcrumb'),
  ];
  $form['semantic_ui']['semantic_ui_popup_description'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Enable form fields popup descriptions'),
    '#default_value' => theme_get_setting('semantic_ui_popup_description'),
  ];
  $form['semantic_ui']['semantic_ui_fixed_navbar'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Enable fixed navigation bar'),
    '#default_value' => theme_get_setting('semantic_ui_fixed_navbar'),
  ];
  $form['semantic_ui']['semantic_ui_error_popups'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Enable form field error popups'),
    '#default_value' => theme_get_setting('semantic_ui_error_popups'),
  ];
  $form['semantic_ui']['semantic_ui_floating_labels'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Enable text field floating labels'),
    '#default_value' => theme_get_setting('semantic_ui_floating_labels'),
  ];
  $form['semantic_ui']['semantic_ui_enlarge_touchable_font'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Enlarge font size on touch devices'),
    '#default_value' => theme_get_setting('semantic_ui_enlarge_touchable_font'),
  ];
}
